package it.niedermann.owncloud.notes.manageaccounts;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;
import static it.niedermann.owncloud.notes.branding.BrandingUtil.applyBrandToLayerDrawable;
import static it.niedermann.owncloud.notes.shared.util.ApiVersionUtil.getPreferredApiVersion;

import android.graphics.drawable.LayerDrawable;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.PopupMenu;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import it.niedermann.owncloud.notes.R;
import it.niedermann.owncloud.notes.databinding.ItemAccountChooseBinding;
import it.niedermann.owncloud.notes.persistence.entity.Account;
import it.niedermann.owncloud.notes.shared.util.AccountSyncUtil;
import it.niedermann.owncloud.notes.shared.util.DisplayUtils;

public class ManageAccountViewHolder extends RecyclerView.ViewHolder {

    private final ItemAccountChooseBinding binding;

    public ManageAccountViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = ItemAccountChooseBinding.bind(itemView);
    }

    public void bind(
            @NonNull Account localAccount,
            @NonNull IManageAccountsCallback callback,
            boolean isCurrentAccount
    ) {
        binding.accountName.setText(localAccount.getUserName());
        DisplayUtils.setHost(localAccount, binding.accountHost);
        Glide.with(itemView.getContext())
                .load(DisplayUtils.getAvatarUrl(localAccount))
                .error(R.drawable.ic_account_circle_grey_32dp)
                .apply(RequestOptions.circleCropTransform())
                .into(binding.accountItemAvatar);
        itemView.setOnClickListener((v) -> callback.onSelect(localAccount));
        binding.accountContextMenu.setVisibility(VISIBLE);
        binding.accountContextMenu.setOnClickListener((v) -> {
            final var popup = new PopupMenu(itemView.getContext(), v);
            popup.inflate(R.menu.menu_account);

            final var preferredApiVersion = getPreferredApiVersion(localAccount.getApiVersion());

            boolean isDeviceLocalAccount =  AccountSyncUtil.isLocalAccount(itemView.getContext(), localAccount);

            if (preferredApiVersion == null || !preferredApiVersion.supportsFileSuffixChange() || isDeviceLocalAccount) {
                popup.getMenu().removeItem(popup.getMenu().findItem(R.id.file_suffix).getItemId());
            }

            if (preferredApiVersion == null || !preferredApiVersion.supportsNotesPathChange() || isDeviceLocalAccount) {
                popup.getMenu().removeItem(popup.getMenu().findItem(R.id.notes_path).getItemId());
            }

            if (AccountSyncUtil.isMurenaAccount(itemView.getContext(), localAccount.getAccountName())) {
                popup.getMenu().removeItem(popup.getMenu().findItem(R.id.delete).getItemId());
            }

            popup.setOnMenuItemClickListener(item -> {
                if (item.getItemId() == R.id.notes_path) {
                    callback.onChangeNotesPath(localAccount);
                    return true;
                } else if (item.getItemId() == R.id.file_suffix) {
                    callback.onChangeFileSuffix(localAccount);
                    return true;
                } else if (item.getItemId() == R.id.delete) {
                    callback.onDelete(localAccount);
                    return true;
                }
                return false;
            });
            popup.show();
        });
        if (isCurrentAccount) {
            binding.currentAccountIndicator.setVisibility(VISIBLE);
            applyBrandToLayerDrawable((LayerDrawable) binding.currentAccountIndicator.getDrawable(), R.id.area, localAccount.getColor());
        } else {
            binding.currentAccountIndicator.setVisibility(GONE);
        }
    }
}
