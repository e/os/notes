package it.niedermann.owncloud.notes.accountswitcher;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.core.util.Consumer;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import it.niedermann.owncloud.notes.R;
import it.niedermann.owncloud.notes.databinding.ItemAccountChooseBinding;
import it.niedermann.owncloud.notes.persistence.entity.Account;
import it.niedermann.owncloud.notes.shared.util.DisplayUtils;

public class AccountSwitcherViewHolder extends RecyclerView.ViewHolder {

    ItemAccountChooseBinding binding;

    public AccountSwitcherViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = ItemAccountChooseBinding.bind(itemView);
    }

    public void bind(@NonNull Account localAccount, @NonNull Consumer<Account> onAccountClick) {
        binding.accountName.setText(localAccount.getDisplayName());
        DisplayUtils.setHost(localAccount, binding.accountHost);
        Glide.with(itemView.getContext())
                .load(DisplayUtils.getAvatarUrl(localAccount))
                .placeholder(R.drawable.ic_account_circle_grey_32dp)
                .error(R.drawable.ic_account_circle_grey_32dp)
                .apply(RequestOptions.circleCropTransform())
                .into(binding.accountItemAvatar);
        itemView.setOnClickListener((v) -> onAccountClick.accept(localAccount));
        binding.accountContextMenu.setVisibility(View.GONE);
    }
}
