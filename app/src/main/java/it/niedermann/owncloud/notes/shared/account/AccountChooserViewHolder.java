package it.niedermann.owncloud.notes.shared.account;

import android.net.Uri;

import androidx.core.util.Consumer;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import it.niedermann.owncloud.notes.R;
import it.niedermann.owncloud.notes.databinding.ItemAccountChooseBinding;
import it.niedermann.owncloud.notes.persistence.entity.Account;
import it.niedermann.owncloud.notes.shared.util.DisplayUtils;

public class AccountChooserViewHolder extends RecyclerView.ViewHolder {
    private final ItemAccountChooseBinding binding;

    protected AccountChooserViewHolder(ItemAccountChooseBinding binding) {
        super(binding.getRoot());
        this.binding = binding;
    }

    public void bind(Account localAccount, Consumer<Account> targetAccountConsumer) {
        Glide
                .with(binding.accountItemAvatar.getContext())
                .load(DisplayUtils.getAvatarUrl(localAccount))
                .placeholder(R.drawable.ic_account_circle_grey_32dp)
                .error(R.drawable.ic_account_circle_grey_32dp)
                .apply(RequestOptions.circleCropTransform())
                .into(binding.accountItemAvatar);

        binding.accountLayout.setOnClickListener((v) -> targetAccountConsumer.accept(localAccount));
        binding.accountName.setText(localAccount.getDisplayName());
        binding.accountHost.setText(Uri.parse(localAccount.getUrl()).getHost());
    }
}