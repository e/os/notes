# Notes
An android client for [Nextcloud Notes App](https://github.com/nextcloud/notes/)  
Notes is forked from [Nextcloud Notes](https://github.com/stefan-niedermann/nextcloud-notes)

## Authors

[Authors](https://gitlab.e.foundation/e/apps/Notes/-/blob/master/AUTHORS)

## Release Notes

Check out the [Release Notes](https://gitlab.e.foundation/e/apps/Notes/-/releases) to find out what changed
in each version of Notes.

## Privacy Policy

[Privacy Policy](https://e.foundation/legal-notice-privacy)  
[Terms of service](https://e.foundation/legal-notice-privacy)

## License

Notes is licensed under the [GNU General Public License v3.0](https://gitlab.e.foundation/e/apps/Notes/-/blob/master/LICENSE)